import open from 'open';

export let openUrl = async (urlArg: string) => {
  if (!(process.env.CI === 'true')) {
    const childProcess = await open(urlArg, {
      wait: false,
    });
    return childProcess;
  } else {
    return null;
  }
};
